resource "aws_s3_bucket" "aws_bucket_name" {
  bucket = "${s3bucketname}"

  tags = {
    Name        = "${s3bucketname}"
    Environment = "Dev"
  }
}

terraform {
  required_version = "1.3.1"
  backend "s3" {
    region         = "ap-southeast-2"
    bucket         = "terraform-state-bucket-imed-dev"
    key            = "s3-${s3bucketname}.tfstate"
    dynamodb_table = "terraform-lock"
  }
}
